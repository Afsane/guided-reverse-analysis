import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.*;

import soot.*;
import soot.JastAddJ.Variable;
import soot.jimple.AddExpr;
import soot.jimple.AssignStmt;
import soot.jimple.Constant;
import soot.jimple.DefinitionStmt;
import soot.jimple.EqExpr;
import soot.jimple.GeExpr;
import soot.jimple.GotoStmt;
import soot.jimple.GtExpr;
import soot.jimple.IfStmt;
import soot.jimple.IntConstant;
import soot.jimple.Jimple;
import soot.jimple.LeExpr;
import soot.jimple.LtExpr;
import soot.jimple.MulExpr;
import soot.jimple.NeExpr;
import soot.jimple.NewArrayExpr;
import soot.jimple.NopStmt;
import soot.jimple.Stmt;
import soot.jimple.internal.AbstractNewArrayExpr;
import soot.jimple.internal.JAddExpr;
import soot.jimple.internal.JArrayRef;
import soot.jimple.internal.JAssignStmt;
import soot.jimple.internal.JGotoStmt;
import soot.jimple.internal.JIfStmt;
import soot.jimple.internal.JLengthExpr;
import soot.jimple.internal.JMulExpr;
import soot.jimple.internal.JNewArrayExpr;
import soot.jimple.internal.JNopStmt;
import soot.jimple.internal.JRetStmt;
import soot.jimple.internal.JReturnStmt;
import soot.jimple.internal.JReturnVoidStmt;
import soot.jimple.internal.JimpleLocal;
import soot.tagkit.*;
import soot.toolkits.graph.*;
import soot.toolkits.scalar.ArraySparseSet;
import soot.toolkits.scalar.BackwardFlowAnalysis;
import soot.toolkits.scalar.FlowSet;
import soot.toolkits.scalar.ForwardBranchedFlowAnalysis;
import soot.toolkits.scalar.ForwardFlowAnalysis;



public class MyMain 
{
	public AbsEnvironment a1;
	public static List<String> Ass = new ArrayList<String>();
	public static Interval tempN;
	public static Interval tempP;
	public static Interval tempI;
	public static Interval temp;
	
	public static void main(String[] args) 
	{
		
		PackManager.v().getPack("jtp").add(
				new Transform("jtp.myTransform", new BodyTransformer() {

					protected void internalTransform(Body body, String phase, Map options) 
					{
						SootMethod m = body.getMethod();
						Iterator<Tag> i = m.getTags().iterator();
						System.out.println(System.currentTimeMillis());
						while(i.hasNext())
						{
							Tag t = i.next();
							if(t.getName() == "VisibilityAnnotationTag")
							{
								
								//System.out.println(t.toString());
								String[] s = t.toString().split("Annotation Element: kind:");
								/*for(String st: s)
									System.out.println(st);*/
								for(int j=0;j<s.length; j++)
								{
									//System.out.println(s[j]);
									if(s[j].contains(" value"))
									{
										String [] s1 = s[j].split(" value:");
										for(int c=1;c<s1.length;c=c+2)
										{
											s1[c] =s1[c].replace(',', ' ').replace(']', ' ');
											String p= s1[c].trim();
											if(p != "," || p!="]")
											{
												//System.out.println(p);
												Ass.add(p);
											}
										}
									}
								}
									
							}
							
						}
						//System.out.println("Method: "+body.getMethod().toString());
						//if(body.getMethod().toString().toLowerCase().contains("main"))
						if(body.getMethod().toString().toLowerCase().contains("main") || body.getMethod().toString().toLowerCase().contains("execute"))
						{
							ExceptionalUnitGraph G = new ExceptionalUnitGraph(body);
							System.out.println("Base Analysis starts: "+System.currentTimeMillis());
							new MyAnalysis(G);			
							System.out.println("Base Analysis ends: "+System.currentTimeMillis());
							//new RA(G);
							//System.out.println("Reverse Analysis ends: "+System.currentTimeMillis());
							//new GRA(G);
							System.out.println("Guided reverse Analysis ends: "+System.currentTimeMillis());
						}
						System.out.println(System.currentTimeMillis());
					}
					
				}));
		
		soot.Main.main(args);
	}
	
	public static class RA extends BackwardFlowAnalysis<Unit, AbsEnvironment>
	{
		public int q;
		public int a;
		public int b;
		public int c;
		public int d;
		PrintWriter writer;
		public Interval r_x1;
		AbsEnvironment emptySet = new AbsEnvironment();
		public RA(UnitGraph g) 
		{
			super(g);
			try {
				writer = new PrintWriter("/Users/Afsane/RAnalysisResult.txt");
			} catch (FileNotFoundException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			G.v().out.println("BACKWARD");
			writer.println("Reverse Analysis");
			doAnalysis();
			writer.flush();
			// TODO Auto-generated constructor stub
		}

		@Override
		protected void flowThrough(AbsEnvironment in, Unit d, AbsEnvironment out) 
		{
			// TODO Auto-generated method stub
            System.out.println("**********************************");
			System.out.println("Unit "+ d);
            writer.println("**********************************");
			writer.println("Unit "+ d);
			if(d instanceof JNopStmt)
			{
				JNopStmt n= (JNopStmt) d;
				n.count++;
				out.copy(in);      	 
           	 	G.v().out.println("-----IN-----");
           	 	writer.println("-----IN-----");
    			in.print();           	 
    			writer.println(in.printS());
    			G.v().out.println("-----OUT-----");
    			writer.println("-----OUT-----");
    			out.print();
    			writer.println(out.printS());
            	n.prevOut.copy(out);
			}
			else if(d instanceof JAssignStmt)
			{
				JAssignStmt n= (JAssignStmt) d;
				String key = n.getLeftOp().toString();
				//G.v().out.print(((AssignStmt) d).getLeftOp() + "=");
				java.util.Iterator<ValueBox> vbIt = d.getUseBoxes().iterator();
				while(vbIt.hasNext())
				{
		            ValueBox vb = (ValueBox) vbIt.next();
		            Value v = vb.getValue();
		            if(v instanceof NopStmt)
		            {
		           	 	G.v().out.println("-----IN-----");
		    			in.print();     
		           	 	writer.println("-----IN-----");
		    			writer.println(in.printS());  
		    			out.copy(in);      	 
		    			G.v().out.println("-----OUT-----");
		    			out.print();
		    			writer.println("-----OUT-----");
		    			writer.println(out.printS());
		            }
		          /*  else if(v instanceof AbstractNewArrayExpr)
		            {
		           	 	G.v().out.println("-----IN-----");
		    			in.print(); 
		    			
		            	//AbstractNewArrayExpr ae = (AbstractNewArrayExpr)v;
		    			out.copy(in);      	 
		    			G.v().out.println("-----OUT-----");
		    			out.print();
		            }*/
		            /*else if(v instanceof JMulExpr)
		            {
	                	 JMulExpr ae = (JMulExpr) v;				                
			             Value lo = ae.getOp1(), ro = ae.getOp2();
			               
	                	 if(lo instanceof IntConstant)
	                	 {
	                		 int l = Integer.parseInt(lo.toString());
	                		 int h = l;
	                		 Interval i = new intInterval(l,h);	                		 
	                		 in.vars.put(lo.toString(), i);
	                	 }
	                	 if(ro instanceof IntConstant)
	                	 {
	                		 int l = Integer.parseInt(ro.toString());
	                		 int h = l;
	                		 Interval i = new intInterval(l,h);	                		 
	                		 in.vars.put(ro.toString(), i);
	                	 }

			           	if(!in.vars.containsKey(lo.toString()))
			           	{
			           		temp = new Top();
			           		in.vars.put(lo.toString(),temp);
			           	}
			           	if(!in.vars.containsKey(ro.toString()))
			           	{
			           		temp = new Top();
			           		in.vars.put(ro.toString(),temp);
			           	}
	                	 
			            G.v().out.println("-----IN-----");
			           	in.print(); 
			           	writer.println("-----IN-----");
			           	writer.println(in.printS()); 
		            	if(in.vars.isEmpty() || n.l.vars.isEmpty())
		    				out.copy(in);      	 
		            	else if(!in.vars.isEmpty() && !n.l.vars.isEmpty())
		            	{
		    				out.copy(in);    
		    				G.v().out.println("----Previous----");
		    				n.l.print();
		    				writer.println("----Previous----");
		    				writer.println(n.l.printS());
	            			temp = new Top();
	            			out.vars.put(key, temp);
	            			if( lo.toString() != ro.toString())
	            			{
	            				r_x1 = ReverseMultiplication(in.vars.get(key),n.l.vars.get(ro.toString())).intersection(in.vars.get(lo.toString()));
	            				temp = r_x1;
	            				out.vars.put(lo.toString(), temp);
	            				temp = ReverseMultiplication(in.vars.get(key),r_x1).intersection(in.vars.get(ro.toString()));	            			
	            				out.vars.put(ro.toString(), temp);	
	            			}
	            			else
	            			{
	            				temp = ReverseSquare(n.l.vars.get(key),in.vars.get(key),n.l.vars.get(lo.toString()),in.vars.get(lo.toString()));
            					out.vars.put(lo.toString(), temp);	
	            			}

	            		}
               		 	in.vars.remove(lo.toString());
               		 	in.vars.remove(ro.toString());
		    			G.v().out.println("-----OUT-----");
		    			out.print();
		    			writer.println("-----OUT-----");
		    			writer.println(out.printS());
		            }*/
		            else if(v instanceof JAddExpr)
		            {
	                	 JAddExpr ae = (JAddExpr) v;				                
			             Value lo = ae.getOp1(), ro = ae.getOp2();
			               
	                	 if(lo instanceof IntConstant)
	                	 {
	                		 int l = Integer.parseInt(lo.toString());
	                		 int h = l;
	                		 Interval i = new intInterval(l,h);	                		 
	                		 in.vars.put(lo.toString(), i);
	                	 }
	                	 if(ro instanceof IntConstant)
	                	 {
	                		 int l = Integer.parseInt(ro.toString());
	                		 int h = l;
	                		 Interval i = new intInterval(l,h);	                		 
	                		 in.vars.put(ro.toString(), i);
	                	 }

			           	if(!in.vars.containsKey(lo.toString()))
			           	{
			           		temp = new Top();
			           		in.vars.put(lo.toString(),temp);
			           	}
			           	if(!in.vars.containsKey(ro.toString()))
			           	{
			           		temp = new Top();
			           		in.vars.put(ro.toString(),temp);
			           	}
	                	 
			            G.v().out.println("-----IN-----");
			           	in.print(); 
			            writer.println("-----IN-----");
			           	writer.println(in.printS()); 
		            	if(in.vars.isEmpty() && n.l.vars.isEmpty())
		    				out.copy(in);      	 
		            	else if(!in.vars.isEmpty())
		            	{
		            		out.copy(in);      	 
		    				if( in.vars.get(key).low == Integer.MIN_VALUE &&  in.vars.get(ro.toString()).low >0)
		    				{
		    					this.a = Math.max(in.vars.get(lo.toString()).low, in.vars.get(key).low);
		    				}
		    				else
		    				{
		    					this.a = Math.max(in.vars.get(lo.toString()).low, in.vars.get(key).low - in.vars.get(ro.toString()).low);
		    				}
		    				if( in.vars.get(key).high == Integer.MAX_VALUE &&  in.vars.get(ro.toString()).low <0) //july 16
		    				{
			            		this.q = Math.min(in.vars.get(lo.toString()).high, in.vars.get(key).high+ a);
		    				}
		    				else
		    				{
		    					this.q = Math.min(in.vars.get(lo.toString()).high, in.vars.get(key).high- in.vars.get(key).low + a);
		    				}
		            		this.b = Math.min(q, in.vars.get(key).high- in.vars.get(ro.toString()).low);
		            		this.c = Math.max(in.vars.get(ro.toString()).low, in.vars.get(key).low-a);
		            		this.d = Math.min(in.vars.get(ro.toString()).high, in.vars.get(key).high-b);
		            		/*if(in.vars.get(key).high < in.vars.get(lo.toString()).low + in.vars.get(ro.toString()).low) july 16
		            			out.vars.clear();
		            		else if(in.vars.get(key).low > in.vars.get(lo.toString()).high + in.vars.get(ro.toString()).high)
		            			out.vars.clear();
		            		else 
		            		{*/
		            			temp = new Top();
		            			out.vars.put(key, temp);
		            			temp = new intInterval(this.a,this.b);
		            			out.vars.put(lo.toString(), temp);
		            			temp = new intInterval(this.c,this.d);
		            			out.vars.put(ro.toString(), temp);
		            		//}
		            	}
               		 	in.vars.remove(lo.toString());
               		 	in.vars.remove(ro.toString());
		    			G.v().out.println("-----OUT-----");
		    			out.print();
		    			writer.println("-----OUT-----");
		    			writer.println(out.printS());
		            }
	                 else if(v instanceof IntConstant && vbIt.hasNext()== false)
	                 {
	                	 G.v().out.println("-----IN-----");
	         			 in.print();
	                	 writer.println("-----IN-----");
	         			 writer.println(in.printS());
	         			 out.copy(in);
	         			 temp = new Top();
	         			 out.vars.put(key,temp);
	                	 G.v().out.println("-----out-----");
	                	 out.print();
	                	 writer.println("-----out-----");
	                	 writer.println(out.printS());
	                 }
	                 else if(((AssignStmt) d).getRightOp() instanceof Local)
	                 {
	                	G.v().out.println("-----IN-----");
	                	in.print();
	                	writer.println("-----IN-----");
	                	writer.println(in.printS());
	                	temp = new Top();
	                	String s = ((AssignStmt) d).getRightOp().toString();
			           	if(!in.vars.containsKey(s))
			           	{
			           		temp = new Top();
			           		in.vars.put(s,temp);
			           	}
	                	out.copy(in);
	                	out.vars.put(s, in.vars.get(key).intersection(in.vars.get(s)));
		                out.vars.put(key,temp);
	                	G.v().out.println("-----OUT-----");
	                	out.print();
	                	writer.println("-----OUT-----");
	                	writer.println(out.printS());
	                 }
				}
			}
			else if(d instanceof JIfStmt)
			{
				JIfStmt n= (JIfStmt) d;
				n.rcount++;
				out.copy(in);      	 
           	 	G.v().out.println("-----IN-----");
    			in.print();  
           	 	writer.println("-----IN-----");
    			writer.println(in.printS()); 
    			if(n.rcount>3)
    			{
        			G.v().out.println("-----prevOut-----");
        			n.prevOut.print();
    				out.copy(widen(n.prevOut,out));
    			}
    			G.v().out.println("-----OUT-----");
    			out.print();
    			writer.println("-----OUT-----");
    			writer.println(out.printS());
            	n.prevOut = out.clone();
			}
			else if(d instanceof JReturnVoidStmt)
			{
				JReturnVoidStmt n= (JReturnVoidStmt) d;
				assertInput(in);
            	G.v().out.println("-----IN-----");
            	in.print();
            	writer.println("-----IN-----");
            	writer.println(in.printS());
            	out.copy(in);

            	G.v().out.println("-----OUT-----");
            	out.print();
            	writer.println("-----OUT-----");
            	writer.println(out.printS());
			}
			else if(d instanceof JGotoStmt)
			{
				if(in.vars.isEmpty())
					assertInput(in);
            	G.v().out.println("-----IN-----");
            	in.print();
            	writer.println("-----IN-----");
            	writer.println(in.printS());
            	out.copy(in);
            	G.v().out.println("-----OUT-----");
            	out.print();
            	writer.println("-----OUT-----");
            	writer.println(out.printS());
			}
		}

		public void assertInput( AbsEnvironment in)
		{
			for(int c=0; c<Ass.size(); c+=3)
        	{
        		if(Ass.get(c+1) == "<=")
        		{
        			tempN = new negInf(Integer.parseInt(Ass.get(c+2)));
        			//tempN.high = Integer.parseInt(Ass.get(c+2));
        			in.vars.put(Ass.get(c),tempN);
        		}
        		else if(Ass.get(c+1) == "<")
        		{
        			tempN = new negInf(Integer.parseInt(Ass.get(c+2))-1);
        			//tempN.high = Integer.parseInt(Ass.get(c+2))-1;
        			in.vars.put(Ass.get(c),tempN);
        		}
        		else if(Ass.get(c+1).equals(">="))
        		{
        			tempP = new posInf(Integer.parseInt(Ass.get(c+2)));
        			//tempP.low = Integer.parseInt(Ass.get(c+2));
        			in.vars.put(Ass.get(c),tempP);
        		}
        		else if(Ass.get(c+1) == ">")
        		{
        			tempP = new posInf(Integer.parseInt(Ass.get(c+2))+1);
        			//tempP.low = Integer.parseInt(Ass.get(c+2))+1;
        			in.vars.put(Ass.get(c),tempP);
        		}
        		else if(Ass.get(c+1) == "==")
        		{
        			tempI = new intInterval(Integer.parseInt(Ass.get(c+2)),Integer.parseInt(Ass.get(c+2)));
        			//tempI.low = Integer.parseInt(Ass.get(c+2));
        			//tempI.high = Integer.parseInt(Ass.get(c+2));
        			in.vars.put(Ass.get(c),tempI);
        		}
        	}
		}
		
		public Interval ReverseSquare(Interval lx3,Interval mx3,Interval lx1,Interval mx1)
		{ 
			Double a = null,b=null;
			Interval temp,t;
			t = new intInterval(lx1.low*lx1.low, lx1.high*lx1.high);
			if(!(lx1.intersection(mx1).equal(lx1)) || !(lx3.intersection(mx3).equal(lx3)) || !(t.intersection(mx3).equal(t)))
			{
				temp = new Bottom();
			}
			else 
			{
				if(mx3.low ==0)
				{	
					if(mx3.high ==Integer.MAX_VALUE)
						a= -1 * Double.MAX_VALUE;
					else
						a= -1 * Math.sqrt(mx3.high);
					b= Math.sqrt(mx3.high);
				}
				else if(lx1.low>0 && mx3.low>0)
				{
					if(mx3.low ==Integer.MAX_VALUE)
						a= Double.MAX_VALUE;
					else
						a= Math.sqrt(mx3.low);
					if(mx3.high ==Integer.MAX_VALUE)
						b= Double.MAX_VALUE;
					else
						b=Math.sqrt(mx3.high);
				}
				else if(lx1.high<0 && mx3.low>0)
				{
					if(mx3.high ==Integer.MAX_VALUE)
						a= -1 * Double.MAX_VALUE;
					else
						a= -1* Math.sqrt(mx3.high);
					if(mx3.low ==Integer.MAX_VALUE)
						b= -1 * Double.MAX_VALUE;
					else
						b= -1*Math.sqrt(mx3.low);
				}
				if(a !=null && b!=null)
				{
					if(a<= b)
					{
						if(a<=Integer.MIN_VALUE && b >= Integer.MAX_VALUE)
							temp = new Top();
						else if(a<= Integer.MIN_VALUE)
							temp = new negInf(b.intValue());
						else if(b>=Integer.MAX_VALUE)
							temp = new posInf(a.intValue());
						else
							temp = new intInterval(a.intValue(),b.intValue());
					}
					else
						temp= new Bottom();
				}
				else
				{
					temp = new Bottom();
				}
				
			}
			return temp;
		}
		
		public Interval ReverseMultiplication(Interval m,Interval d)
		{
			Interval temp;
			int i,s;
			i = Integer.MIN_VALUE;
			if(m.high>=0 && d.low <0)
				i = (int) Math.max(i, Math.ceil(m.high/d.low));
			else if(m.low<=0 && d.high>0)
				i = (int) Math.max(i, Math.ceil(m.low/d.high));
			else if(m.low >= 0 && d.low>0)
				i = (int) Math.max(i, Math.ceil(m.low/d.low));
			else if(m.high<=0 && d.high<0)
				i = (int) Math.max(i, Math.ceil(m.high/d.high));
			else if((m.low==0 && d.high>0) ||(m.high==0 && d.low<0))
				i= Math.max(i, 0);
			s = Integer.MAX_VALUE;
			if(m.low>=0 && d.high<0)
				s = (int) Math.min(s, Math.floor(m.low/d.high));
			else if(m.high<=0 && d.low >0)
				s = (int) Math.min(s, Math.floor(m.high/d.low));
			else if(m.high >= 0 && d.high>0)
				s = (int) Math.min(s, Math.floor(m.high/d.high));
			else if(m.low<=0 && d.low<0)
				s = (int) Math.min(s, Math.floor(m.low/d.low));
			else if((m.low==0 && d.low<0) ||(m.high==0 && d.high>0))
				s= Math.min(s,0);
			if(i<= s)
			{
				if(i== Integer.MIN_VALUE && s == Integer.MAX_VALUE)
					temp = new Top();
				else if(i== Integer.MIN_VALUE)
					temp = new negInf(s);
				else if(s == Integer.MAX_VALUE)
					temp = new posInf(i);
				else
					temp = new intInterval(i,s);
			}
			else
				temp = new Bottom();
			return temp;
		}
		//@SuppressWarnings("unchecked")
		@Override
		protected void copy(AbsEnvironment source, AbsEnvironment dest) 
		{
			dest.copy(source);
		}

		@Override
		protected AbsEnvironment entryInitialFlow() {
			// TODO Auto-generated method stub
			return emptySet.clone();
		}
   
		@Override
		protected void merge(AbsEnvironment in1, AbsEnvironment in2,AbsEnvironment out) {
			// TODO Auto-generated method stub
			G.v().out.println("MERGE");
			writer.println("MERGE");

			in1.meet(in2, out);// changed meet to join june 7
			for(String key: in1.vars.keySet())
			{
				if(!out.vars.containsKey(key))
					out.vars.put(key, in1.vars.get(key));
			}
			for(String key: in2.vars.keySet())
			{
				if(!out.vars.containsKey(key))
					out.vars.put(key, in2.vars.get(key));
			}
		}

		@Override
		protected AbsEnvironment newInitialFlow() {
			// TODO Auto-generated method stub
			return emptySet.clone();
		}
		
	    public AbsEnvironment widen(AbsEnvironment prevOut,AbsEnvironment out)
	    {
	    	G.v().out.println("widen");
	    	Interval temp;
	    	for(String key: out.vars.keySet())
	    	{
	    		if(prevOut.vars.get(key).low <= out.vars.get(key).low)
	    			if(out.vars.get(key).high <= prevOut.vars.get(key).high)
	    				temp = new intInterval(prevOut.vars.get(key).low , prevOut.vars.get(key).high);
	    			else 
	    				temp = new posInf(prevOut.vars.get(key).low);
	    		else if(out.vars.get(key).high <= prevOut.vars.get(key).high)
	    			temp = new negInf(prevOut.vars.get(key).high);
	    		else
	    			temp = new Top();
	    		out.vars.put(key,temp);
	    	}
	    	return out;
	    }
	}

	

	public static class GRA extends BackwardFlowAnalysis<Unit, AbsEnvironment>
	{
		public int q;
		public int a;
		public int b;
		public int c;
		public int d;
		PrintWriter writer;
		public Interval r_x1;
		AbsEnvironment emptySet = new AbsEnvironment();
		public GRA(UnitGraph g) 
		{
			super(g);
			try {
				writer = new PrintWriter("/Users/Afsane/GRAnalysisResult.txt");
			} catch (FileNotFoundException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			G.v().out.println("BACKWARD");
			writer.println("Guided Reverse Analysis");
			doAnalysis();
			writer.flush();
			// TODO Auto-generated constructor stub
		}

		
		@Override
		protected void flowThrough(AbsEnvironment in, Unit d, AbsEnvironment out) 
		{
			// TODO Auto-generated method stub
            System.out.println("**********************************");
			System.out.println("Unit "+ d);
            writer.println("**********************************");
			writer.println("Unit "+ d);
			if(d instanceof JNopStmt)
			{
				JNopStmt n= (JNopStmt) d;
				n.count++;
				out.copy(in);      	 
           	 	G.v().out.println("-----IN-----");
           	 	writer.println("-----IN-----");
    			in.print();           	 
    			writer.println(in.printS());
    			G.v().out.println("-----OUT-----");
    			writer.println("-----OUT-----");
    			out.print();
    			writer.println(out.printS());
            	n.prevOut.copy(out);
			}
			else if(d instanceof JAssignStmt)
			{
				JAssignStmt n= (JAssignStmt) d;
				String key = n.getLeftOp().toString();
				//G.v().out.print(((AssignStmt) d).getLeftOp() + "=");
				java.util.Iterator<ValueBox> vbIt = d.getUseBoxes().iterator();
				while(vbIt.hasNext())
				{
		            ValueBox vb = (ValueBox) vbIt.next();
		            Value v = vb.getValue();
		            if(v instanceof NopStmt)
		            {
		           	 	G.v().out.println("-----IN-----");
		    			in.print();     
		           	 	writer.println("-----IN-----");
		    			writer.println(in.printS());  
		    			out.copy(in);      	 
		    			G.v().out.println("-----OUT-----");
		    			out.print();
		    			writer.println("-----OUT-----");
		    			writer.println(out.printS());
		            }
		          /*  else if(v instanceof AbstractNewArrayExpr)
		            {
		           	 	G.v().out.println("-----IN-----");
		    			in.print(); 
		    			
		            	//AbstractNewArrayExpr ae = (AbstractNewArrayExpr)v;
		    			out.copy(in);      	 
		    			G.v().out.println("-----OUT-----");
		    			out.print();
		            }*/
		            else if(v instanceof JMulExpr)
		            {
	                	 JMulExpr ae = (JMulExpr) v;				                
			             Value lo = ae.getOp1(), ro = ae.getOp2();
			               
	                	 if(lo instanceof IntConstant)
	                	 {
	                		 int l = Integer.parseInt(lo.toString());
	                		 int h = l;
	                		 Interval i = new intInterval(l,h);	                		 
	                		 in.vars.put(lo.toString(), i);
	                	 }
	                	 if(ro instanceof IntConstant)
	                	 {
	                		 int l = Integer.parseInt(ro.toString());
	                		 int h = l;
	                		 Interval i = new intInterval(l,h);	                		 
	                		 in.vars.put(ro.toString(), i);
	                	 }

			           	if(!in.vars.containsKey(lo.toString()))
			           	{
			           		temp = new Top();
			           		in.vars.put(lo.toString(),temp);
			           	}
			           	if(!in.vars.containsKey(ro.toString()))
			           	{
			           		temp = new Top();
			           		in.vars.put(ro.toString(),temp);
			           	}
	                	 
			            G.v().out.println("-----IN-----");
			           	in.print(); 
			           	writer.println("-----IN-----");
			           	writer.println(in.printS()); 
		            	if(in.vars.isEmpty() || n.l.vars.isEmpty())
		    				out.copy(in);      	 
		            	else if(!in.vars.isEmpty() && !n.l.vars.isEmpty())
		            	{
		    				out.copy(in);    
		    				G.v().out.println("----Previous----");
		    				n.l.print();
		    				writer.println("----Previous----");
		    				writer.println(n.l.printS());
	            			temp = new Top();
	            			out.vars.put(key, temp);
	            			if( lo.toString() != ro.toString())
	            			{
	            				r_x1 = ReverseMultiplication(in.vars.get(key),n.l.vars.get(ro.toString())).intersection(in.vars.get(lo.toString()));
	            				temp = r_x1;
	            				out.vars.put(lo.toString(), temp);
	            				temp = ReverseMultiplication(in.vars.get(key),r_x1).intersection(in.vars.get(ro.toString()));	            			
	            				out.vars.put(ro.toString(), temp);	
	            			}
	            			else
	            			{
	            				temp = ReverseSquare(n.l.vars.get(key),in.vars.get(key),n.l.vars.get(lo.toString()),in.vars.get(lo.toString()));
            					out.vars.put(lo.toString(), temp);	
	            			}

	            		}
               		 	in.vars.remove(lo.toString());
               		 	in.vars.remove(ro.toString());
		    			G.v().out.println("-----OUT-----");
		    			out.print();
		    			writer.println("-----OUT-----");
		    			writer.println(out.printS());
		            }
		            else if(v instanceof JAddExpr)
		            {
	                	 JAddExpr ae = (JAddExpr) v;				                
			             Value lo = ae.getOp1(), ro = ae.getOp2();
			               
	                	 if(lo instanceof IntConstant)
	                	 {
	                		 int l = Integer.parseInt(lo.toString());
	                		 int h = l;
	                		 Interval i = new intInterval(l,h);	                		 
	                		 in.vars.put(lo.toString(), i);
	                	 }
	                	 if(ro instanceof IntConstant)
	                	 {
	                		 int l = Integer.parseInt(ro.toString());
	                		 int h = l;
	                		 Interval i = new intInterval(l,h);	                		 
	                		 in.vars.put(ro.toString(), i);
	                	 }

			           	if(!in.vars.containsKey(lo.toString()))
			           	{
			           		temp = new Top();
			           		in.vars.put(lo.toString(),temp);
			           	}
			           	if(!in.vars.containsKey(ro.toString()))
			           	{
			           		temp = new Top();
			           		in.vars.put(ro.toString(),temp);
			           	}
	                	 
			            G.v().out.println("-----IN-----");
			           	in.print(); 
			            writer.println("-----IN-----");
			           	writer.println(in.printS()); 
		            	if(in.vars.isEmpty() && n.l.vars.isEmpty())
		    				out.copy(in);      	 
		            	else if(!in.vars.isEmpty() && n.l.vars.isEmpty())
		            	{
		            		out.copy(in);      	 
		            		this.a = Math.max(in.vars.get(lo.toString()).low, in.vars.get(key).low - in.vars.get(ro.toString()).low);
		            		this.q = Math.min(in.vars.get(lo.toString()).high, in.vars.get(key).high- in.vars.get(key).low + a);
		            		this.b = Math.min(q, in.vars.get(key).high- in.vars.get(ro.toString()).low);
		            		this.c = Math.max(in.vars.get(ro.toString()).low, in.vars.get(key).low-a);
		            		this.d = Math.min(in.vars.get(ro.toString()).high, in.vars.get(key).high-b);
		            		if(in.vars.get(key).high < in.vars.get(lo.toString()).low + in.vars.get(ro.toString()).low)
		            			out.vars.clear();
		            		else if(in.vars.get(key).low > in.vars.get(lo.toString()).high + in.vars.get(ro.toString()).high)
		            			out.vars.clear();
		            		else 
		            		{
		            			temp = new Top();
		            			out.vars.put(key, temp);
		            			temp = new intInterval(this.a,this.b);
		            			out.vars.put(lo.toString(), temp);
		            			temp = new intInterval(this.c,this.d);
		            			out.vars.put(ro.toString(), temp);
		            		}
		            	}
		            	else if(!in.vars.isEmpty() && !n.l.vars.isEmpty())
		            	{
		    				out.copy(in);    
		    				G.v().out.println("----Previous----");
		    				n.l.print();
		    				writer.println("----Previous----");
		    				writer.println(n.l.printS());
		    				if( in.vars.get(key).low == Integer.MIN_VALUE &&  n.l.vars.get(ro.toString()).low >0)
		    				{
		    					this.a = Math.max(in.vars.get(lo.toString()).low, in.vars.get(key).low);
		    				}
		    				else
		    				{
		    					this.a = Math.max(in.vars.get(lo.toString()).low, in.vars.get(key).low - n.l.vars.get(ro.toString()).low);
		    				}
		    				if( in.vars.get(key).high == Integer.MAX_VALUE &&  in.vars.get(ro.toString()).high <0) //july 16
		    				{
		    					this.b = Math.min(in.vars.get(lo.toString()).high, in.vars.get(key).high);
		    				}
		    				else
		    				{
		    					this.b = Math.min(in.vars.get(lo.toString()).high, in.vars.get(key).high- n.l.vars.get(ro.toString()).high);
		    				}
		            		this.c = in.vars.get(key).low -a;
		            		this.d = in.vars.get(key).high-b;
	            			temp = new Top();
	            			out.vars.put(key, temp);
	            			temp = new intInterval(this.a,this.b);
	            			out.vars.put(lo.toString(), temp);
	            			temp = new intInterval(this.c,this.d);	            			
	            			out.vars.put(ro.toString(), temp.intersection(in.vars.get(ro.toString())));	
	            		}
               		 	in.vars.remove(lo.toString());
               		 	in.vars.remove(ro.toString());
		    			G.v().out.println("-----OUT-----");
		    			out.print();
		    			writer.println("-----OUT-----");
		    			writer.println(out.printS());
		            }
	                 else if(v instanceof IntConstant && vbIt.hasNext()== false)
	                 {
	                	 G.v().out.println("-----IN-----");
	         			 in.print();
	                	 writer.println("-----IN-----");
	         			 writer.println(in.printS());
	         			 out.copy(in);
	         			 temp = new Top();
	         			 out.vars.put(key,temp);
	                	 G.v().out.println("-----out-----");
	                	 out.print();
	                	 writer.println("-----out-----");
	                	 writer.println(out.printS());
	                 }
	                 else if(((AssignStmt) d).getRightOp() instanceof Local)
	                 {
	                	G.v().out.println("-----IN-----");
	                	in.print();
	                	writer.println("-----IN-----");
	                	writer.println(in.printS());
	                	temp = new Top();
	                	String s = ((AssignStmt) d).getRightOp().toString();
			           	if(!in.vars.containsKey(s))
			           	{
			           		temp = new Top();
			           		in.vars.put(s,temp);
			           	}
	                	out.copy(in);
	                	if(in.vars.containsKey(key))
	                	{
	                		out.vars.put(s, in.vars.get(key).intersection(in.vars.get(s)));
	                	}
		                out.vars.put(key,temp);
	                	G.v().out.println("-----OUT-----");
	                	out.print();
	                	writer.println("-----OUT-----");
	                	writer.println(out.printS());
	                 }
				}
			}
			else if(d instanceof JIfStmt)
			{
				JIfStmt n= (JIfStmt) d;
				n.rcount++;
				out.copy(in);      	 
           	 	G.v().out.println("-----IN-----");
    			in.print();  
           	 	writer.println("-----IN-----");
    			writer.println(in.printS()); 
    			if(n.rcount>3)
    			{
        			G.v().out.println("-----prevOut-----");
        			n.prevOut.print();
    				out.copy(widen(n.prevOut,out));
    			}
    			G.v().out.println("-----OUT-----");
    			out.print();
    			writer.println("-----OUT-----");
    			writer.println(out.printS());
            	n.prevOut = out.clone();
			}
			else if(d instanceof JReturnVoidStmt)
			{
				JReturnVoidStmt n= (JReturnVoidStmt) d;
				assertInput(in);
            	G.v().out.println("-----IN-----");
            	in.print();
            	writer.println("-----IN-----");
            	writer.println(in.printS());
            	out.copy(in);

            	G.v().out.println("-----OUT-----");
            	out.print();
            	writer.println("-----OUT-----");
            	writer.println(out.printS());
			}
			else if(d instanceof JGotoStmt)
			{
				if(in.vars.isEmpty())
					assertInput(in);
            	G.v().out.println("-----IN-----");
            	in.print();
            	writer.println("-----IN-----");
            	writer.println(in.printS());
            	out.copy(in);
            	G.v().out.println("-----OUT-----");
            	out.print();
            	writer.println("-----OUT-----");
            	writer.println(out.printS());
			}
		}

		public void assertInput( AbsEnvironment in)
		{
			
			for(int c=0; c<Ass.size(); c+=3)
        	{
				
        		if(Ass.get(c+1).equals("<="))
        		{
        			tempN = new negInf(Integer.parseInt(Ass.get(c+2)));
        			//tempN.high = Integer.parseInt(Ass.get(c+2));
        			in.vars.put(Ass.get(c),tempN);
        		}
        		else if(Ass.get(c+1).equals("<"))
        		{
        			tempN = new negInf(Integer.parseInt(Ass.get(c+2))-1);
        			//tempN.high = Integer.parseInt(Ass.get(c+2))-1;
        			in.vars.put(Ass.get(c),tempN);
        		}
        		else if(Ass.get(c+1).equals(">="))
        		{
        			tempP = new posInf(Integer.parseInt(Ass.get(c+2)));
        			//tempP.low = Integer.parseInt(Ass.get(c+2));
        			in.vars.put(Ass.get(c),tempP);
        		}
        		else if(Ass.get(c+1).equals(">"))
        		{
        			tempP = new posInf(Integer.parseInt(Ass.get(c+2))+1);
        			//tempP.low = Integer.parseInt(Ass.get(c+2))+1;
        			in.vars.put(Ass.get(c),tempP);
        		}
        		else if(Ass.get(c+1).equals("=="))
        		{
        			tempI = new intInterval(Integer.parseInt(Ass.get(c+2)),Integer.parseInt(Ass.get(c+2)));
        			//tempI.low = Integer.parseInt(Ass.get(c+2));
        			//tempI.high = Integer.parseInt(Ass.get(c+2));
        			in.vars.put(Ass.get(c),tempI);
        		}
        	}
		}
		
		public Interval ReverseSquare(Interval lx3,Interval mx3,Interval lx1,Interval mx1)
		{ 
			Double a = null,b=null;
			Interval temp,t;
			t = new intInterval(lx1.low*lx1.low, lx1.high*lx1.high);
			if(!(lx1.intersection(mx1).equal(lx1)) || !(lx3.intersection(mx3).equal(lx3)) || !(t.intersection(mx3).equal(t)))
			{
				temp = new Bottom();
			}
			else 
			{
				if(mx3.low ==0)
				{	
					if(mx3.high ==Integer.MAX_VALUE)
						a= -1 * Double.MAX_VALUE;
					else
						a= -1 * Math.sqrt(mx3.high);
					b= Math.sqrt(mx3.high);
				}
				else if(lx1.low>0 && mx3.low>0)
				{
					if(mx3.low ==Integer.MAX_VALUE)
						a= Double.MAX_VALUE;
					else
						a= Math.sqrt(mx3.low);
					if(mx3.high ==Integer.MAX_VALUE)
						b= Double.MAX_VALUE;
					else
						b=Math.sqrt(mx3.high);
				}
				else if(lx1.high<0 && mx3.low>0)
				{
					if(mx3.high ==Integer.MAX_VALUE)
						a= -1 * Double.MAX_VALUE;
					else
						a= -1* Math.sqrt(mx3.high);
					if(mx3.low ==Integer.MAX_VALUE)
						b= -1 * Double.MAX_VALUE;
					else
						b= -1*Math.sqrt(mx3.low);
				}
				if(a !=null && b!=null)
				{
					if(a<= b)
					{
						if(a<=Integer.MIN_VALUE && b >= Integer.MAX_VALUE)
							temp = new Top();
						else if(a<= Integer.MIN_VALUE)
							temp = new negInf(b.intValue());
						else if(b>=Integer.MAX_VALUE)
							temp = new posInf(a.intValue());
						else
							temp = new intInterval(a.intValue(),b.intValue());
					}
					else
						temp= new Bottom();
				}
				else
				{
					temp = new Bottom();
				}
				
			}
			return temp;
		}
		
		public Interval ReverseMultiplication(Interval m,Interval d)
		{
			Interval temp;
			int i,s;
			i = Integer.MIN_VALUE;
			if(m.high>=0 && d.low <0)
				i = (int) Math.max(i, Math.ceil(m.high/d.low));
			else if(m.low<=0 && d.high>0)
				i = (int) Math.max(i, Math.ceil(m.low/d.high));
			else if(m.low >= 0 && d.low>0)
				i = (int) Math.max(i, Math.ceil(m.low/d.low));
			else if(m.high<=0 && d.high<0)
				i = (int) Math.max(i, Math.ceil(m.high/d.high));
			else if((m.low==0 && d.high>0) ||(m.high==0 && d.low<0))
				i= Math.max(i, 0);
			s = Integer.MAX_VALUE;
			if(m.low>=0 && d.high<0)
				s = (int) Math.min(s, Math.floor(m.low/d.high));
			else if(m.high<=0 && d.low >0)
				s = (int) Math.min(s, Math.floor(m.high/d.low));
			else if(m.high >= 0 && d.high>0)
				s = (int) Math.min(s, Math.floor(m.high/d.high));
			else if(m.low<=0 && d.low<0)
				s = (int) Math.min(s, Math.floor(m.low/d.low));
			else if((m.low==0 && d.low<0) ||(m.high==0 && d.high>0))
				s= Math.min(s,0);
			if(i<= s)
			{
				if(i== Integer.MIN_VALUE && s == Integer.MAX_VALUE)
					temp = new Top();
				else if(i== Integer.MIN_VALUE)
					temp = new negInf(s);
				else if(s == Integer.MAX_VALUE)
					temp = new posInf(i);
				else
					temp = new intInterval(i,s);
			}
			else
				temp = new Bottom();
			return temp;
		}
		//@SuppressWarnings("unchecked")
		@Override
		protected void copy(AbsEnvironment source, AbsEnvironment dest) 
		{
			dest.copy(source);
		}

		@Override
		protected AbsEnvironment entryInitialFlow() {
			// TODO Auto-generated method stub
			return emptySet.clone();
		}

		@Override
		protected void merge(AbsEnvironment in1, AbsEnvironment in2,AbsEnvironment out) {
			// TODO Auto-generated method stub
			G.v().out.println("MERGE");
			writer.println("MERGE");

			in1.meet(in2, out);// changed meet to join june 7
			for(String key: in1.vars.keySet())
			{
				if(!out.vars.containsKey(key))
					out.vars.put(key, in1.vars.get(key));
			}
			for(String key: in2.vars.keySet())
			{
				if(!out.vars.containsKey(key))
					out.vars.put(key, in2.vars.get(key));
			}
		}

		@Override
		protected AbsEnvironment newInitialFlow() {
			// TODO Auto-generated method stub
			return emptySet.clone();
		}
		
	    public AbsEnvironment widen(AbsEnvironment prevOut,AbsEnvironment out)
	    {
	    	G.v().out.println("widen");
	    	Interval temp;
	    	for(String key: out.vars.keySet())
	    	{
	    		if(prevOut.vars.get(key).low <= out.vars.get(key).low)
	    			if(out.vars.get(key).high <= prevOut.vars.get(key).high)
	    				temp = new intInterval(prevOut.vars.get(key).low , prevOut.vars.get(key).high);
	    			else 
	    				temp = new posInf(prevOut.vars.get(key).low);
	    		else if(out.vars.get(key).high <= prevOut.vars.get(key).high)
	    			temp = new negInf(prevOut.vars.get(key).high);
	    		else
	    			temp = new Top();
	    		out.vars.put(key,temp);
	    	}
	    	return out;
	    }
	}


	public static class MyAnalysis extends ForwardBranchedFlowAnalysis<AbsEnvironment> 
	{
		int e,f,g,h,t1,t2,t3,t4,t5,t6;
		boolean ifFlag = false;
		AbsEnvironment emptySet = new AbsEnvironment();
		public MyAnalysis(UnitGraph graph) 
		{
			super(graph);
			doAnalysis();
			// TODO Auto-generated constructor stub
		}

		public int multiply(int a, int b)
		{
			int temp;
			if(a == Integer.MAX_VALUE || b == Integer.MAX_VALUE)
				if(a <0 || b<0)
					temp = Integer.MIN_VALUE;
				else
					temp =Integer.MAX_VALUE;
			if(a == Integer.MIN_VALUE || b == Integer.MIN_VALUE)
				if(a <0 || b<0)
					temp = Integer.MIN_VALUE;
				else
					temp = Integer.MAX_VALUE;
			else 
				temp = a*b;
			return temp;
		}
		@Override
		protected void flowThrough(AbsEnvironment in, Unit d,List<AbsEnvironment> fallOut, List<AbsEnvironment> branchOuts) 
		{
			G.v().out.println("**********************************");
			G.v().out.println("Unit "+ d);

			Interval temp = null;
			// TODO Auto-generated method stub
			//G.v().out.println(d);
			
			if(d instanceof JAssignStmt)
			{
				JAssignStmt n= (JAssignStmt) d;
				
				/*String key1 = n.getLeftOp().toString(); // July 13
				//G.v().out.print(((AssignStmt) d).getLeftOp() + "=");
				java.util.Iterator<ValueBox> vbIt1 = d.getUseBoxes().iterator();
				while(vbIt1.hasNext())
				{
		            ValueBox vb = (ValueBox) vbIt1.next();
		            Value v = vb.getValue();
		            if(v instanceof JNewArrayExpr)
		            {
		            	
		            }
		            
				}*/
				String key = n.getLeftOp().toString();
				//G.v().out.println("Type="+ n.getLeftOp().getType());
				//G.v().out.println(key);

				//G.v().out.print(((AssignStmt) d).getLeftOp() + "=");
				java.util.Iterator<ValueBox> vbIt = d.getUseBoxes().iterator();
				while(vbIt.hasNext())
				{
		            ValueBox vb = (ValueBox) vbIt.next();
		            Value v = vb.getValue();
		            //G.v().out.println("---------"+v);
		             if(v instanceof NopStmt)
		             {
		            	 //count++;
	                	 G.v().out.println("-----IN-----");
	         			 in.print();
	         			 n.l = in; // precondition saved in the unit for GRA
	                	 fallOut.get(0).add(in);
	                	 
                		 //G.v().out.println("-----OUT-----");
                		 //fallOut.get(0).print();
		             }		
		             else if(key.contains("["))
					 {
		               	 	G.v().out.println("-----IN-----");
		               	 	in.print();
		                	 fallOut.get(0).add(in);
		         			G.v().out.println("-----fallOut-----");
		        			//G.v().out.println("-----size = "+ fallOut.size());

		        			for(AbsEnvironment a: fallOut)
		        			{
		        				a.print();
		        			}
		        			return;
					}
		          
		             else if(v instanceof JNewArrayExpr)
		             {
		            	 JNewArrayExpr a = (JNewArrayExpr) v;
     					 int l = 0;
     					 int h = Integer.parseInt(a.getSize().toString()) -1;
     					 Interval i = new intInterval(l,h);	  
     					 i.arrayFlag = true;
	         			 fallOut.get(0).add(in);
                		 fallOut.get(0).vars.put(key, i);
     					 //in.vars.put(lo.toString(), i);
		             }
		             else if(v instanceof JArrayRef)
		             {
		            	 JArrayRef j = (JArrayRef) v;
	                	 G.v().out.println("-----IN-----");
	         			 in.print();
/*	     				G.v().out.println("type : "+j.getBase().toString());
	     				G.v().out.println("size : "+j.getIndex().toString());
	     				String s = j.getBase().toString() + " [ " + j.getIndex().toString()+" ]";
	     				G.v().out.println("Inash"+in.vars.get(s));*/
	         			 Interval i = new Top();	         			 
	         			fallOut.get(0).add(in);
	         			fallOut.get(0).vars.put(key, i);
		             }
		             else if(v instanceof JLengthExpr)
		             {
	                	 G.v().out.println("-----IN-----");
	         			 in.print();
		            	 JLengthExpr l = (JLengthExpr) v;
		            	 G.v().out.println(l.getOp());//****************************
		            	 //G.v().out.println(in.vars.get(l.getOp()).high);
		            	 int i = in.vars.get(l.getOp().toString()).high +1;
		            	 Interval i1 = new intInterval(i,i);	
		            	 i1.arrayLengthFlag = true;
	         			 fallOut.get(0).add(in);
		            	 fallOut.get(0).vars.put(key, i1);
		             }		
		             else if(v instanceof JMulExpr)
		             {
	                	 JMulExpr ae = (JMulExpr) v;				                
			             Value lo = ae.getOp1(), ro = ae.getOp2();
			             
	                	 G.v().out.println("-----IN-----");
	         			 in.print();
	         			 if(!in.empty())
	                	 {
	         				 if(lo instanceof IntConstant)
	         				 {
	         					 int l = Integer.parseInt(lo.toString());
	         					 int h = l;
	         					 Interval i = new intInterval(l,h);	                		 
	         					 in.vars.put(lo.toString(), i);
	         					 n.l.vars.put(lo.toString(), i); ///*****************************************
	         				 }
	         				 if(ro instanceof IntConstant)
	         				 {
	         					 int l = Integer.parseInt(ro.toString());
	         					 int h = l;
	         					 Interval i = new intInterval(l,h);	                		 
	         					 in.vars.put(ro.toString(), i);
	         				 }
	         				 fallOut.get(0).add(in);
	         				 if(!in.vars.containsKey(lo.toString()) || !in.vars.containsKey(ro.toString()) || in.vars.get(lo.toString()).getType() == "Bottom" || in.vars.get(ro.toString()).getType() == "Bottom")
	         					 temp = new Bottom();
	         				 
	         				 else if(in.vars.get(lo.toString()).getType() == "Top" || in.vars.get(ro.toString()).getType() == "Top")
	         					 temp = new Top();
	         				 else if((in.vars.get(lo.toString()).getType() == "negInf" || in.vars.get(ro.toString()).getType() == "negInf") && (in.vars.get(lo.toString()).getType() == "posInf" || in.vars.get(ro.toString()).getType() == "posInf"))
		         				 temp = new Top();
	         				 else if(in.vars.get(lo.toString()).getType() == "posInf" && in.vars.get(ro.toString()).getType() == "posInf")
	         				 {
	         					 if(in.vars.get(lo.toString()).low <0 || in.vars.get(ro.toString()).low <0)
	         						 temp = new Top();
	         					 else
	         						 temp= new posInf(in.vars.get(lo.toString()).low * in.vars.get(ro.toString()).low);
	         				 }
	         				 else if(in.vars.get(lo.toString()).getType() == "posInf" && in.vars.get(ro.toString()).getType() == "intInterval")
	         				 {
	         					 if(in.vars.get(ro.toString()).low<0)
	         						 temp = new Top();
	         					 else
	         					 {
	         						 e = in.vars.get(lo.toString()).low * in.vars.get(ro.toString()).low;
	         						 f = in.vars.get(lo.toString()).low * in.vars.get(ro.toString()).high;
	         						 temp = new posInf(Math.min(e,f));
	         					 }
	         				 }
	         				 else if(in.vars.get(ro.toString()).getType() == "posInf" && in.vars.get(lo.toString()).getType() == "intInterval")
	         				 {
	         					 if(in.vars.get(lo.toString()).low<0)
	         						 temp = new Top();
	         					 else
	         					 {
	         						 e = in.vars.get(ro.toString()).low * in.vars.get(lo.toString()).low;
	         						 f = in.vars.get(ro.toString()).low * in.vars.get(lo.toString()).high;
	         						 temp = new posInf(Math.min(e,f));
	         					 }
	         				 }
	         				 else if(in.vars.get(lo.toString()).getType() == "negInf" && in.vars.get(ro.toString()).getType() == "negInf")
	         				 {
	         					 if(in.vars.get(lo.toString()).low <0 && in.vars.get(ro.toString()).low <0)
	         					 {
	         						 e = in.vars.get(ro.toString()).high * in.vars.get(lo.toString()).high;
	         						 temp = new posInf(e);
	         					 }
	         					 else
	         						 temp= new Top();
	         				 }
	         				 else //if((in.vars.get(lo.toString()).getType() == "negInf" || in.vars.get(ro.toString()).getType() == "negInf" ) &&(in.vars.get(lo.toString()).getType() == "intInterval" ||in.vars.get(ro.toString()).getType() == "intInterval" ))
	         				 {
	         					 e = multiply(in.vars.get(lo.toString()).low ,in.vars.get(ro.toString()).low);
	         					 f = multiply(in.vars.get(lo.toString()).low ,in.vars.get(ro.toString()).high);
	         					 g = multiply(in.vars.get(lo.toString()).high ,in.vars.get(ro.toString()).low);
	         					 h = multiply(in.vars.get(lo.toString()).high , in.vars.get(ro.toString()).high);
	         					 t1 = Math.min(e,f);
	         					 t2 = Math.min(t1,g);
	         					 t3 = Math.min(t2,h);
	         					 t4 = Math.max(e,f);
	         					 t5 = Math.max(t4,g);
	         					 t6 = Math.max(t5,h);
	         					 if(t3== Integer.MIN_VALUE && t6 == Integer.MAX_VALUE)
	         						 temp = new Top();
	         					 else if(t3== Integer.MIN_VALUE)
	         						 temp= new negInf(t6);
	         					 else if(t6 == Integer.MAX_VALUE)
	         						 temp = new posInf(t3);
	         					 else
	         						 temp = new intInterval(t3,t6);
	         				 }

	         				 //in.vars.remove(lo.toString());///*****************************************
	         				 //in.vars.remove(ro.toString()); ///*****************************************
	         				 fallOut.get(0).vars.put(key, temp);
	         				 n.l = fallOut.get(0); // precondition saved in the unit for GRA June 7
	                	 } 
	         			 
		             }
		             else if (v instanceof JAddExpr)
		             {
	                	 JAddExpr ae = (JAddExpr) v;				                
			             Value lo = ae.getOp1(), ro = ae.getOp2();
			             n.l = in; // precondition saved in the unit for GRA

	                	 G.v().out.println("-----IN-----");
	         			 in.print();
	         			 //fallOut.get(0).add(in);
	         			 if(!in.empty())
	                	 {
	                	 if(lo instanceof IntConstant)
	                	 {
	                		 int l = Integer.parseInt(lo.toString());
	                		 int h = l;
	                		 Interval i = new intInterval(l,h);	                		 
	                		 in.vars.put(lo.toString(), i);
	                		 n.l.vars.put(lo.toString(), i); ///*****************************************
	                	 }
	                	 if(ro instanceof IntConstant)
	                	 {
	                		 int l = Integer.parseInt(ro.toString());
	                		 int h = l;
	                		 Interval i = new intInterval(l,h);	                		 
	                		 in.vars.put(ro.toString(), i);
	                	 }
	         			 fallOut.get(0).add(in);
	                	 if(!in.vars.containsKey(lo.toString()) || !in.vars.containsKey(ro.toString()) || in.vars.get(lo.toString()).getType() == "Bottom" || in.vars.get(ro.toString()).getType() == "Bottom")
	                		 temp = new Bottom();
	                	 else if(in.vars.get(lo.toString()).getType() == "Top" || in.vars.get(ro.toString()).getType() == "Top")
	                		 temp = new Top();
	                	 else if(in.vars.get(lo.toString()).getType() == "negInf" || in.vars.get(ro.toString()).getType() == "negInf")
	                			 if(in.vars.get(lo.toString()).getType() == "posInf" || in.vars.get(ro.toString()).getType() == "posInf")
	                				 temp = new Top();
	                			 else 
	                				 temp = new negInf(in.vars.get(lo.toString()).high+in.vars.get(ro.toString()).high);			 
	                	 else if(in.vars.get(lo.toString()).getType() == "posInf" || in.vars.get(ro.toString()).getType() == "posInf")
	                		 	temp = new posInf(in.vars.get(lo.toString()).low+in.vars.get(ro.toString()).low);
	                	 else 
	                		 temp = new intInterval(in.vars.get(lo.toString()).low+in.vars.get(ro.toString()).low,in.vars.get(lo.toString()).high+in.vars.get(ro.toString()).high);
	                	 
                		 //in.vars.remove(lo.toString());///*****************************************
                		 //in.vars.remove(ro.toString()); ///*****************************************
                		 fallOut.get(0).vars.put(key, temp);
	                	 }             	 
			         }
	                 else if(v instanceof IntConstant && vbIt.hasNext()== false)
	                 {
	     				n.l = in; // precondition saved in the unit for GRA
	                	 G.v().out.println("-----IN-----");
	         			 in.print();
	         			 fallOut.get(0).add(in);
	                	 int t = Integer.parseInt(v.toString());
	                	 temp = new intInterval(t,t);
	                	 fallOut.get(0).vars.put(key, temp);
	                	 StringTag s = new StringTag("["+t+","+t+"]");
	                	 d.addTag(s);
	                 }
/*		             else if(v instanceof JNewArrayExpr) //// 13 july test
		             {
		            	 JNewArrayExpr t= (JNewArrayExpr) v;
		            	 Value s= t.getSize();
		            	 System.out.println("dhahjhdha");
		             } // July 13
*/	                 else if(((AssignStmt) d).getRightOp() instanceof Local)
	                 {
	                	
	     				n.l = in; // precondition saved in the unit for GRA
	                	G.v().out.println("-----IN-----");
	                	in.print();
	     				/*JimpleLocal j= (JimpleLocal) ((AssignStmt) d).getRightOp(); // July 13
	     				G.v().out.println("type : "+j.getType().toString());
	     				G.v().out.println("size : "+j.getName().toString());
	     				G.v().out.println("size : "+j.getNumber());

	     				if(j.getType().toString().contains("int[]"))
	     				{
	     					
	     					//
	     					///
	     					////
	     					/// to complete
		     				//G.v().out.println("sad");
	     				}*/
	                	
	                	fallOut.get(0).add(in);
	                	if(!in.empty())
	                	{
	                		String s = ((AssignStmt) d).getRightOp().toString();
	                		fallOut.get(0).vars.put(key, in.vars.get(s));
	                	}
	                 }
				}
			}
			else if(d instanceof JIfStmt)
			{
				G.v().out.println("-----IN-----");
            	in.print();
				int l,h;
				Interval temp1;
				JIfStmt i = (JIfStmt)d;
				i.l = in; // precondition saved in the unit for GRA
    	    	i.count++;
				Value v =i.getCondition();
				if(v instanceof EqExpr)
				{
					EqExpr t2 = (EqExpr) v;
	                Value lo = t2.getOp1(), ro = t2.getOp2();
					if(lo instanceof IntConstant)
					{
						if(ro instanceof IntConstant)
						{
							if(!(Integer.parseInt(lo.toString()) == Integer.parseInt(ro.toString())))
							{		
								i.count ++;
								fallOut.get(0).add(in);
								if(i.wFlag == true)
									branchOuts.get(0).add(in);
							}
							else
							{
								branchOuts.get(0).add(in);
							}
						}
						else if(ro instanceof JimpleLocal | ro instanceof Variable)
						{
								if((Integer.parseInt(lo.toString()) == in.vars.get(ro.toString()).high) && (Integer.parseInt(lo.toString()) == in.vars.get(ro.toString()).low))
								{
									branchOuts.get(0).add(in);
								}
								else
								{
									i.count ++;
									fallOut.get(0).add(in);
								}

						}
					}
					else if(ro instanceof IntConstant)
					{
						 	if(lo instanceof JimpleLocal | lo instanceof Variable)
						 	{
						 		if(( in.vars.get(lo.toString()).low == Integer.parseInt(ro.toString())) && ( in.vars.get(lo.toString()).high == Integer.parseInt(ro.toString())))
						 		{
					            	branchOuts.get(0).add(in);								
						 		}
						 		else
						 		{
									i.count ++;
									fallOut.get(0).add(in);
						 		}
						 	}
					}
					else if(lo instanceof JimpleLocal | lo instanceof Variable)
					{
						if(ro instanceof JimpleLocal || ro instanceof Variable)
						{
								if(( in.vars.get(lo.toString()).low == in.vars.get(ro.toString()).low) && ( in.vars.get(lo.toString()).high == in.vars.get(ro.toString()).high))
								{
					            	branchOuts.get(0).add(in);
								}
								else
								{
									i.count++;
									fallOut.get(0).add(in);
								}
						}
					}
				}
				if(v instanceof  NeExpr)
				{
					EqExpr t2 = (EqExpr) v;
	                Value lo = t2.getOp1(), ro = t2.getOp2();
					if(lo instanceof IntConstant)
					{
						if(ro instanceof IntConstant)
						{
							if(!(Integer.parseInt(lo.toString()) != Integer.parseInt(ro.toString())))
							{
								i.count ++;
								fallOut.get(0).add(in);
							}
							else
							{
								branchOuts.get(0).add(in);
							}
						}
						else if(ro instanceof JimpleLocal | ro instanceof Variable)
						{
								if((Integer.parseInt(lo.toString()) != in.vars.get(ro.toString()).high) && (Integer.parseInt(lo.toString()) != in.vars.get(ro.toString()).low))
								{
									branchOuts.get(0).add(in);
								}
								else
								{
									i.count ++;
									fallOut.get(0).add(in);
								}

						}
					}
					else if(ro instanceof IntConstant)
					{
						 	if(lo instanceof JimpleLocal | lo instanceof Variable)
						 	{
						 		if(( in.vars.get(lo.toString()).low != Integer.parseInt(ro.toString())) && ( in.vars.get(lo.toString()).high != Integer.parseInt(ro.toString())))
						 		{
					            	branchOuts.get(0).add(in);								
						 		}
						 		else
						 		{
						 			i.count++;
									fallOut.get(0).add(in);
						 		}
						 	}
					}
					else if(lo instanceof JimpleLocal | lo instanceof Variable)
					{
						if(ro instanceof JimpleLocal || ro instanceof Variable)
						{
								if(( in.vars.get(lo.toString()).low != in.vars.get(ro.toString()).low) && ( in.vars.get(lo.toString()).high != in.vars.get(ro.toString()).high))
								{
					            	branchOuts.get(0).add(in);
								}
								else
								{
									i.count++;
									fallOut.get(0).add(in);
								}
						}
					}
				}
				if(v instanceof GeExpr)
				{
					GeExpr t2 = (GeExpr) v;
	                Value lo = t2.getOp1(), ro = t2.getOp2();
					if(lo instanceof IntConstant)
					{
						if(ro instanceof IntConstant)
						{
							if(!(Integer.parseInt(lo.toString()) >= Integer.parseInt(ro.toString())))
							{
								i.count++;
								fallOut.get(0).add(in);
							}
							{
								branchOuts.get(0).add(in);
							}
						}
						else if(ro instanceof JimpleLocal | ro instanceof Variable)
						{
								if(!(Integer.parseInt(lo.toString()) >= in.vars.get(ro.toString()).high))
								{
									i.count++;
									if(in.vars.get(ro.toString()).arrayLengthFlag == false)
									{
										l= Integer.parseInt(lo.toString())+1;
										temp1 = new posInf(l);
										in.vars.put(ro.toString(), in.vars.get(ro.toString()).intersection(temp1));
									}
									fallOut.get(0).add(in);
								}
								else
								{
									if(in.vars.get(ro.toString()).arrayLengthFlag == false)
									{
										h = Integer.parseInt(lo.toString());
										temp1= new negInf(h);
										in.vars.put(ro.toString(), in.vars.get(ro.toString()).intersection(temp1));
									}
									branchOuts.get(0).add(in);
								}

						}
					}
					else if(ro instanceof IntConstant)
					{
						 	if(lo instanceof JimpleLocal | lo instanceof Variable)
						 	{
						 		if(!( in.vars.get(lo.toString()).low >= Integer.parseInt(ro.toString())))
						 		{
									i.count++;
									if(in.vars.get(lo.toString()).arrayLengthFlag == false)
									{
										h = Integer.parseInt(ro.toString())-1;
										temp1= new negInf(h);
										in.vars.put(lo.toString(), in.vars.get(lo.toString()).intersection(temp1));
									}
									fallOut.get(0).add(in);								
						 		}
						 		else
						 		{
									if(in.vars.get(lo.toString()).arrayLengthFlag == false)
									{
										l= Integer.parseInt(ro.toString());
										temp1 = new posInf(l);
										in.vars.put(lo.toString(), in.vars.get(lo.toString()).intersection(temp1));
									}
									branchOuts.get(0).add(in);
						 		}
						 	}
					}
					else if(lo instanceof JimpleLocal | lo instanceof Variable)
					{
						if(ro instanceof JimpleLocal || ro instanceof Variable)
						{
								if(!( in.vars.get(lo.toString()).low >= in.vars.get(ro.toString()).high))
								{
									if(in.vars.get(lo.toString()).arrayLengthFlag == true)
									{
										l= Integer.parseInt(lo.toString())+1;
										temp1 = new posInf(l);
										in.vars.put(ro.toString(), in.vars.get(ro.toString()).intersection(temp1));
									}
									else if(in.vars.get(ro.toString()).arrayLengthFlag == true)
									{
										h = Integer.parseInt(ro.toString())-1;
										temp1= new negInf(h);
										in.vars.put(lo.toString(), in.vars.get(lo.toString()).intersection(temp1));
									}
									i.count++;
					            	fallOut.get(0).add(in);
								}
								else
								{
									if(in.vars.get(lo.toString()).arrayLengthFlag == true)
									{
										h = Integer.parseInt(lo.toString());
										temp1= new negInf(h);
										in.vars.put(ro.toString(), in.vars.get(ro.toString()).intersection(temp1));
									}
									else if(in.vars.get(ro.toString()).arrayLengthFlag == true)
									{
										l= Integer.parseInt(ro.toString());
										temp1 = new posInf(l);
										in.vars.put(lo.toString(), in.vars.get(lo.toString()).intersection(temp1));
									}
									else
									{
										temp1 = in.vars.get(lo.toString()).intersection(in.vars.get(ro.toString())) ;
										in.vars.put(lo.toString(), temp1);
										in.vars.put(ro.toString(), temp1);
									}
									branchOuts.get(0).add(in);
								}
						}
					}
				}
				if(v instanceof  GtExpr)
				{
					GtExpr t2= (GtExpr) v;
	                Value lo = t2.getOp1(), ro = t2.getOp2();
					if(lo instanceof IntConstant)
					{
						if(ro instanceof IntConstant)
						{
							if(!(Integer.parseInt(lo.toString()) > Integer.parseInt(ro.toString())))
							{
								i.count++;
								fallOut.get(0).add(in);
							}
							else
							{
								branchOuts.get(0).add(in);
							}
						}
						else if(ro instanceof JimpleLocal | ro instanceof Variable)
						{
								if(!(Integer.parseInt(lo.toString()) > in.vars.get(ro.toString()).high))
								{
									i.count++;
									if(in.vars.get(ro.toString()).arrayLengthFlag == false)
									{	
										l = Integer.parseInt(lo.toString());
										temp1= new posInf(l);
										in.vars.put(ro.toString(), in.vars.get(ro.toString()).intersection(temp1));
									}
									fallOut.get(0).add(in);							
								}
								else
								{
									if(in.vars.get(ro.toString()).arrayLengthFlag == false)
									{
										h = Integer.parseInt(lo.toString()) -1;
										temp1 = new negInf(h);
										in.vars.put(ro.toString(), in.vars.get(ro.toString()).intersection(temp1));
									}
									branchOuts.get(0).add(in);
								}

						}
					}
					else if(ro instanceof IntConstant)
					{
						 	if(lo instanceof JimpleLocal | lo instanceof Variable)
						 	{
						 		if(!( in.vars.get(lo.toString()).low > Integer.parseInt(ro.toString())))
						 		{
									i.count++;
									if(in.vars.get(lo.toString()).arrayLengthFlag == false)
									{
										h = Integer.parseInt(ro.toString());
										temp1= new negInf(h);
										in.vars.put(lo.toString(), in.vars.get(lo.toString()).intersection(temp1));
									}
									fallOut.get(0).add(in);				
						 		}
						 		else
						 		{
									if(in.vars.get(lo.toString()).arrayLengthFlag == false)
									{
										l= Integer.parseInt(ro.toString()) + 1;
										temp1 = new posInf(l);
										in.vars.put(lo.toString(), in.vars.get(lo.toString()).intersection(temp1));
									}
									branchOuts.get(0).add(in);
						 		}

						 	}
					}
					else if(lo instanceof JimpleLocal | lo instanceof Variable)
					{
						if(ro instanceof JimpleLocal || ro instanceof Variable)
						{
								if(!( in.vars.get(lo.toString()).low > in.vars.get(ro.toString()).high))
								{
									if(in.vars.get(lo.toString()).arrayLengthFlag == true)
									{
										l = Integer.parseInt(lo.toString());
										temp1= new posInf(l);
										in.vars.put(ro.toString(), in.vars.get(ro.toString()).intersection(temp1));
									}
									else if(in.vars.get(ro.toString()).arrayLengthFlag == true)
									{
										h = Integer.parseInt(ro.toString());
										temp1= new negInf(h);
										in.vars.put(lo.toString(), in.vars.get(lo.toString()).intersection(temp1));
									}				
									i.count++;
									fallOut.get(0).add(in);
								}
								else
								{
									if(in.vars.get(lo.toString()).arrayLengthFlag == true)
									{
										h = Integer.parseInt(lo.toString()) -1;
										temp1 = new negInf(h);
										in.vars.put(ro.toString(), in.vars.get(ro.toString()).intersection(temp1));
									}
									else if(in.vars.get(ro.toString()).arrayLengthFlag == true)
									{
										l= Integer.parseInt(ro.toString()) + 1;
										temp1 = new posInf(l);
										in.vars.put(lo.toString(), in.vars.get(lo.toString()).intersection(temp1));
									}	
									else
									{
										temp1 = in.vars.get(lo.toString()).intersection(in.vars.get(ro.toString())) ;
										in.vars.put(lo.toString(), temp1);
										in.vars.put(ro.toString(), temp1);
									}
									branchOuts.get(0).add(in);
								}
						}
					}
				}
				if(v instanceof  LtExpr)
				{
					LtExpr t2 = (LtExpr) v;
	                Value lo = t2.getOp1(), ro = t2.getOp2();
					if(lo instanceof IntConstant)
					{
						if(ro instanceof IntConstant)
						{
							if(!(Integer.parseInt(lo.toString()) < Integer.parseInt(ro.toString())))
							{
								i.count++;
								fallOut.get(0).add(in);
							}
							else
				            	branchOuts.get(0).add(in);
						}
						else if(ro instanceof JimpleLocal | ro instanceof Variable)
						{
								if(!(Integer.parseInt(lo.toString()) < in.vars.get(ro.toString()).low))
								{
									i.count++;
									if(in.vars.get(ro.toString()).arrayLengthFlag == false)
									{
										h = Integer.parseInt(lo.toString());
										temp1= new negInf(h);
										in.vars.put(ro.toString(), in.vars.get(ro.toString()).intersection(temp1));
									}
									fallOut.get(0).add(in);
								}
								else
								{
									if(in.vars.get(ro.toString()).arrayLengthFlag == false)
									{
										l= Integer.parseInt(lo.toString())+1;
										temp1 = new posInf(l);
										in.vars.put(ro.toString(), in.vars.get(ro.toString()).intersection(temp1));
									}
					            	branchOuts.get(0).add(in);

								}

						}
					}
					else if(ro instanceof IntConstant)
					{
						 	if(lo instanceof JimpleLocal | lo instanceof Variable)
						 	{
						 		if(!( in.vars.get(lo.toString()).high < Integer.parseInt(ro.toString())))
						 		{
						 			
									i.count++;
									if(in.vars.get(lo.toString()).arrayLengthFlag == false)
									{
										l= Integer.parseInt(ro.toString());
										temp1 = new posInf(l);
										in.vars.put(lo.toString(), in.vars.get(lo.toString()).intersection(temp1));
									}
									//branchOuts.get(0).add(in);
									fallOut.get(0).add(in);
						 		}
						 		else
						 		{
									if(in.vars.get(lo.toString()).arrayLengthFlag == false)
									{
										h = Integer.parseInt(ro.toString())-1;
										temp1= new negInf(h);
										in.vars.put(lo.toString(), in.vars.get(lo.toString()).intersection(temp1));
									}
					            	//fallOut.get(0).add(in);
									branchOuts.get(0).add(in);

						 		}
						 	}
					}
					else if(lo instanceof JimpleLocal | lo instanceof Variable)
					{
						if(ro instanceof JimpleLocal || ro instanceof Variable)
						{
								if(!( in.vars.get(lo.toString()).high < in.vars.get(ro.toString()).low))
								{
									if(in.vars.get(lo.toString()).arrayLengthFlag == true)
									{
										i.count++;
										h = in.vars.get(lo.toString()).low;
										temp1 = new negInf(h);
										in.vars.put(ro.toString(), in.vars.get(ro.toString()).intersection(temp1));
									}
									else if(in.vars.get(ro.toString()).arrayLengthFlag == true)
									{
										i.count++;
										l= in.vars.get(ro.toString()).low;
										temp1 = new posInf(l);
										in.vars.put(lo.toString(), in.vars.get(lo.toString()).intersection(temp1));
									}
									else
									{
										i.count++;
									}
									fallOut.get(0).add(in);
								}
								else
								{
									////OCT 18
									if(in.vars.get(lo.toString()).arrayLengthFlag == true)
									{
										l= in.vars.get(lo.toString()).low+1;
										temp1 = new posInf(l);
										in.vars.put(ro.toString(), in.vars.get(ro.toString()).intersection(temp1));
									}
									else if(in.vars.get(ro.toString()).arrayLengthFlag == true)
									{
							 			h = in.vars.get(ro.toString()).low -1;
							 			temp1= new negInf(h);
							 			in.vars.put(lo.toString(), in.vars.get(lo.toString()).intersection(temp1));
									}
									else
									{
										temp1 = in.vars.get(lo.toString()).intersection(in.vars.get(ro.toString())) ;
										in.vars.put(lo.toString(), temp1);
										in.vars.put(ro.toString(), temp1);
									}
									branchOuts.get(0).add(in);
								}

						}
					}
				}
				if(v instanceof  LeExpr)
				{
					LeExpr t2 = (LeExpr) v;
	                Value lo = t2.getOp1(), ro = t2.getOp2();
					if(lo instanceof IntConstant)
					{
						if(ro instanceof IntConstant)
						{
							if(!(Integer.parseInt(lo.toString()) <= Integer.parseInt(ro.toString())))
							{
								i.count++;
								fallOut.get(0).add(in);
							}
							else
							{
				            	branchOuts.get(0).add(in);
							}
						}
						else if(ro instanceof JimpleLocal | ro instanceof Variable)
						{
								if(!(Integer.parseInt(lo.toString()) <= in.vars.get(ro.toString()).low))
								{
									if(in.vars.get(ro.toString()).arrayLengthFlag == false)
									{
										h = Integer.parseInt(lo.toString()) - 1;
										temp1= new negInf(h);
										in.vars.put(ro.toString(), in.vars.get(ro.toString()).intersection(temp1));
									}
									in.count ++;
									fallOut.get(0).add(in);
									
								}
								else
								{
									if(in.vars.get(ro.toString()).arrayLengthFlag == false)
									{
										l= Integer.parseInt(lo.toString());
										temp1 = new posInf(l);
										in.vars.put(ro.toString(), in.vars.get(ro.toString()).intersection(temp1));
									}
					            	branchOuts.get(0).add(in);
								}
						}
					}
					else if(ro instanceof IntConstant)
					{
						 	if(lo instanceof JimpleLocal | lo instanceof Variable)
						 	{
						 		if(!( in.vars.get(lo.toString()).high <= Integer.parseInt(ro.toString())))
						 		{
						 			
						 			AbsEnvironment a = new AbsEnvironment();/////// Added june 7
						 			a.copy(in);/////// Added june 7
									//i.count++; /////// Added june 7
									if(in.vars.get(lo.toString()).arrayLengthFlag == false)
									{
										l= Integer.parseInt(ro.toString()) +1;
										temp1 = new posInf(l);
										a.vars.put(lo.toString(), a.vars.get(lo.toString()).intersection(temp1));
									}/////// modified june 7
									//branchOuts.get(0).add(in);
									fallOut.get(0).add(a);
/*									if(i.wFlag == true) /////// Added june 7
									{
							 			h = Integer.parseInt(ro.toString());
							 			temp1= new negInf(h);
							 			in.vars.get(lo.toString()).high= Integer.parseInt(ro.toString());
							 			in.vars.put(lo.toString(), in.vars.get(lo.toString()).intersection(temp1));
										branchOuts.get(0).add(in);
									}*/
						 		}
						 		else
						 		{
						 			i.count++; /////// Added june 7
									if(in.vars.get(lo.toString()).arrayLengthFlag == false)
									{
										h = Integer.parseInt(ro.toString());
										temp1= new negInf(h);
										in.vars.put(lo.toString(), in.vars.get(lo.toString()).intersection(temp1));
									}
									branchOuts.get(0).add(in);
						 		}

						 	}
					}
					else if(lo instanceof JimpleLocal | lo instanceof Variable)
					{
						if(ro instanceof JimpleLocal || ro instanceof Variable)
						{
								if(!( in.vars.get(lo.toString()).high <= in.vars.get(ro.toString()).low))
								{
									if(in.vars.get(lo.toString()).arrayLengthFlag == true)
									{
										i.count++;
										h = in.vars.get(lo.toString()).low - 1;
										temp1= new negInf(h);
										in.vars.put(ro.toString(), in.vars.get(ro.toString()).intersection(temp1));
									}
									else if(in.vars.get(ro.toString()).arrayLengthFlag == true)
									{
										i.count++;
										l= in.vars.get(ro.toString()).low +1;
										temp1 = new posInf(l);
										in.vars.put(lo.toString(), in.vars.get(lo.toString()).intersection(temp1));
									}
									else
									{
										i.count++;
									}
									fallOut.get(0).add(in);
								}
								else
								{
									if(in.vars.get(lo.toString()).arrayLengthFlag == true)
									{
										l= in.vars.get(lo.toString()).low;
										temp1 = new posInf(l);
										in.vars.put(ro.toString(), in.vars.get(ro.toString()).intersection(temp1));
									}
									else if(in.vars.get(ro.toString()).arrayLengthFlag == true)
									{
										h = in.vars.get(ro.toString()).low;
							 			temp1= new negInf(h);
							 			in.vars.put(lo.toString(), in.vars.get(lo.toString()).intersection(temp1));
									}
									else
									{
										temp1 = in.vars.get(lo.toString()).intersection(in.vars.get(ro.toString())) ;
										in.vars.put(lo.toString(), temp1);
										in.vars.put(ro.toString(), temp1);
									}
									branchOuts.get(0).add(in);
								}
						}
					}	
				}

			}
			else if(d instanceof JNopStmt)
			{						    
				List<Unit> t1 = graph.getSuccsOf(d);
				int j = t1.size();
				if(j>0)
				{
					
					if(t1.get(j-1) instanceof JIfStmt)
					{
						JIfStmt f = (JIfStmt) t1.get(j-1);						
						ifFlag = true;
					    f.wFlag = true; // after if there's a nop
					}
				}
				JNopStmt n= (JNopStmt) d;
				
				n.count++;		
				n.l= in; // precondition saved in the unit for GRA
            	G.v().out.println("-----IN-----");
            	in.print();
            	fallOut.get(0).add(in);
  			  	//if(n.count > 3 && ifFlag == true && n.wCount ==0)	//Added june 7	
            	if(n.count > 3 && ifFlag == true)
            	{
  			  		//n.wCount++; //Added june 7	
        			G.v().out.println("-----prevOut-----");
        			n.prevOut.print();
  				  	fallOut.get(0).add(widen(n.prevOut,fallOut.get(0)));
  			  	}
			  	ifFlag = false;
            	n.prevOut= fallOut.get(0).clone(); //june 7
			  	//n.prevOut = in; //june7
			}	
			else if(d instanceof JGotoStmt)
			{
				JGotoStmt n= (JGotoStmt) d;
				n.l = in;// precondition saved in the unit for GRA
            	G.v().out.println("-----IN-----");
            	in.print();
            	
            	if(!branchOuts.isEmpty())
            	{
            		branchOuts.get(0).add(in);
            	}
            	else
            		branchOuts.add(in);
			}
			else if(d instanceof  JReturnVoidStmt)
			{
				JReturnVoidStmt n= (JReturnVoidStmt) d;
				n.l = in; // precondition saved in the unit for GRA
            	G.v().out.println("-----IN-----");
            	in.print();
            	if(!fallOut.isEmpty())
            	{
            		fallOut.get(0).add(in);
            	}
            	else
            		fallOut.add(in);
			}

			G.v().out.println("-----fallOut-----");
			//G.v().out.println("-----size = "+ fallOut.size());

			for(AbsEnvironment a: fallOut)
			{
				a.print();
			}
			
			G.v().out.println("-----branchOut-----");

			for(AbsEnvironment b: branchOuts)
			{
				b.print();
			}
			// TODO Auto-generated method stub
			
		}
		
	    public AbsEnvironment widen(AbsEnvironment prevOut,AbsEnvironment out)
	    {
	    	G.v().out.println("widen");
	    	Interval temp;
	    	for(String key: out.vars.keySet())
	    	{
	    		if(prevOut.vars.get(key).low <= out.vars.get(key).low)
	    			if(out.vars.get(key).high <= prevOut.vars.get(key).high)
	    				temp = new intInterval(prevOut.vars.get(key).low , prevOut.vars.get(key).high);
	    			else 
	    				temp = new posInf(prevOut.vars.get(key).low);
	    		else if(out.vars.get(key).high <= prevOut.vars.get(key).high)
	    			temp = new negInf(prevOut.vars.get(key).high);
	    		else
	    			temp = new Top();
	    		out.vars.put(key,temp);
	    	}
	    	return out;
	    }

		@Override
		protected void copy(AbsEnvironment source, AbsEnvironment dest) {
			// TODO Auto-generated method stub
			dest.clear();
			dest.vars = (HashMap<String, Interval>) source.vars.clone();
		}

		@Override
		protected AbsEnvironment entryInitialFlow() {
			// TODO Auto-generated method stub
			return emptySet.clone();
		}

		@Override
		protected void merge(AbsEnvironment in1, AbsEnvironment in2,AbsEnvironment out) {
			// TODO Auto-generated method stub			
			G.v().out.println("MERGE");
			in1.join(in2, out);

		}

		@Override
		protected AbsEnvironment newInitialFlow() {
			// TODO Auto-generated method stub
			return emptySet.clone();
		}
		
	}
}