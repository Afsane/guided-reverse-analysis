package soot;
import java.util.*;

import soot.G;
import soot.Value;
import soot.ValueBox;
import soot.toolkits.scalar.FlowSet;


public class AbsEnvironment 
{
	public HashMap<String,Interval> vars = new HashMap<String,Interval>();
	public int count;
	
	public AbsEnvironment(){};
	
	public void join(AbsEnvironment v, AbsEnvironment out)
	{
        if(!out.equals(this) & !out.equals(v))
		{
            out.add(this);
            java.util.Iterator<String> i = v.vars.keySet().iterator();
            while(i.hasNext())
            {
                String key = i.next();
                if(out.vars.containsKey(key))
                {
                    if (!out.vars.get(key).equal(v.vars.get(key)))
                        out.vars.put(key, out.vars.get(key).join(v.vars.get(key)));
                }
                else
                    out.vars.put(key,v.vars.get(key));
            }
		}
		else if(!out.equals(this))
		{
			for(String key: this.vars.keySet())
			{
				if(out.vars.containsKey(key))
				{
					if (!out.vars.get(key).equal(this.vars.get(key)))
						out.vars.put(key, out.vars.get(key).join(this.vars.get(key)));
				}
				else
					out.vars.put(key,this.vars.get(key));
			}
		}
		else
		{
			for(String key: this.vars.keySet())
			{
				if(out.vars.containsKey(key))
				{
					if (!out.vars.get(key).equal(v.vars.get(key)))
						out.vars.put(key, out.vars.get(key).join(v.vars.get(key)));
				}
				else
					out.vars.put(key,v.vars.get(key));
			}
		}
		/*for(String key :v.vars.keySet())
		{
			if(out.vars.containsKey(key))
				out.vars.put(key, out.vars.get(key).join(v.vars.get(key)));
			else
				out.vars.put(key,v.vars.get(key));
		}*/
	}
    
    public void meet(AbsEnvironment v, AbsEnvironment out)
    {
        out.clear();
        for(String key: this.vars.keySet())
        {
            if(v.vars.containsKey(key))
                out.vars.put(key,this.vars.get(key).meet(v.vars.get(key)));
        }
    }
	
    public AbsEnvironment clone()
	{
		AbsEnvironment temp = new AbsEnvironment();
		temp.copy(this); /////MODIFIED May 31
		return temp;
	}
	
	public void add(AbsEnvironment a)
	{
		//this.vars.putAll(a.vars);
		java.util.Iterator<String> i = a.vars.keySet().iterator();
		while(i.hasNext())
		{
			String key = i.next();
			if(this.vars.containsKey(key))
			{
				if (!this.vars.get(key).equal(a.vars.get(key)))
					this.vars.put(key, a.vars.get(key));
			}
			else
				this.vars.put(key,a.vars.get(key));
		}
	}
	
	public int size()
	{
		return vars.size();
	}
	  public boolean equals(Object o) 
	  {
		    if (!(o instanceof AbsEnvironment)) return false;
		    AbsEnvironment other = (AbsEnvironment)o;
		    if (size() != other.size()) return false;
		    for(String key: vars.keySet())
		      if (!other.vars.containsKey(key)) return false;
		      else if(!vars.get(key).equal(other.vars.get(key))) return false;
		    return true;
	 }
    
    public boolean empty()
    {
        if(vars.size() == 0)
            return true;
        else
            return false;
    }
    
    public void clear()
    {
        if(!this.empty())
            this.vars.clear();
    }
    
    public void copy(AbsEnvironment a)
    {
        this.vars = (HashMap<String, Interval>) a.vars.clone();
    }
	
	public void print()
	{
        for(String key: this.vars.keySet())
        {
            if(this.vars.get(key).getType() == "Bottom")
            {
                G.v().out.println(key +"= Bottom");
            }
            else
            G.v().out.println(key +"= ["+this.vars.get(key).low +","+this.vars.get(key).high+"]");
        }
    
		//java.util.Iterator<String> i = this.vars.keySet().iterator();
		//while(i.hasNext())
		//{
			//String key = i.next();
   		 //	G.v().out.println(key +"= ["+this.vars.get(key).low +","+this.vars.get(key).high+"]");
		//}
	}
    
    public String printS()
	{
        String s = new String();
        for(String key: this.vars.keySet())
        {
            if(this.vars.get(key).getType() == "Bottom")
            {
                s += key +"= Bottom";
            }
            else
                s += key +"= ["+this.vars.get(key).low +","+this.vars.get(key).high+"]";
        }
        return s;
		//java.util.Iterator<String> i = this.vars.keySet().iterator();
		//while(i.hasNext())
		//{
        //String key = i.next();
        //	G.v().out.println(key +"= ["+this.vars.get(key).low +","+this.vars.get(key).high+"]");
		//}
	}
	
}