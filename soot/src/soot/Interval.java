package soot;

public abstract class Interval
{
	public int low,high;
    public boolean arrayLengthFlag;

	//int pI= 2147483647,nI = -2147483648;
	public Interval(){};
	public Interval(int l, int h)
	{
		low =l;
		high =h;
	};
	public boolean equal(Interval a)
	{
		if(low != a.low)
			return false;
		if(high != a.high)
			return false;
		return true;
	}
	public abstract String getType();
	protected abstract Interval clone();
	public abstract Interval join(Interval i);
    public abstract Interval meet(Interval i);
	public abstract Interval intersection(Interval i);
}
