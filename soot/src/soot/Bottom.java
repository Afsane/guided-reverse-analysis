package soot;


public class Bottom extends Interval
{
	public Bottom()
	{
		super();
	}
	public String getType()
	{
		return "Bottom";
	}
	
	public Interval clone()
	{
		Interval i = new Bottom();
		return i;
	}
	
	public Interval join(Interval i)
	{
		Interval temp;
		if(i.getType()=="Bottom")
			temp = new Bottom();
		else
			temp =i.clone();
		return temp;
	}
	
	public Interval intersection(Interval i)
	{
		Interval temp = new Bottom();
		return temp;
	}
    
    public Interval meet(Interval i)
	{
		Interval temp = new Bottom();
		return temp;
	}
}
