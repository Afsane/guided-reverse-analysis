package soot;


public class negInf extends Interval
{
	
	public negInf(int h)
	{
		super(-2147483648,h);
		//low = -2147483648;
		//high = h;
	}
	
	public String getType()
	{
		return "negInf";
	}
	
	public int getHigh()
	{
		return high;
	}
	
	public Interval clone()
	{
		Interval i = new negInf(high);
		return i;
	}
	
	public Interval join(Interval i)
	{
		Interval temp;
		if(i.getType() == "Bottom")
			temp = this.clone();
		else if(i.getType() == "Top" || i.getType() =="posInf")
			temp = new Top();
		else
		{
			if(i.high> this.high)
				temp = new negInf(i.high);
			else
				temp = new negInf(this.high);
		}
		return temp;
	}
    
    public Interval meet(Interval i)
	{
		int l,h;
		Interval temp ;
		if(i.getType() == "Top")
			temp =this.clone();
		else if(i.getType() =="Bottom")
			temp = new Bottom();
		else if(intersect(i))
		{
			if(i.getType() == "posInf")
			{
				l= i.low;
				h= this.high;
				temp = new intInterval(l,h);
			}
			else if(i.getType() == "negInf")
			{
				if(this.high< i.high)
					h= this.high;
				else
					h= i.high;
				temp = new negInf(h);
			}
			else
			{
				l= i.low;
				if(this.high<i.high)
					h= this.high;
				else
					h=i.high;
				temp = new intInterval(l,h);
			}
		}
		else
			temp = new Bottom();
		return temp;
	}
	
	public Interval intersection(Interval i)
	{
		int l,h;
		Interval temp ;
		if(i.getType() == "Top")
			temp = this.clone();
		else if(i.getType() =="Bottom")
			temp = new Bottom();
		else if(intersect(i))
		{
			if(i.getType() == "posInf")
			{
				l= i.low;
				h= this.high;
				temp = new intInterval(l,h);
			}
			else if(i.getType() == "negInf")
			{
				if(this.high< i.high)
					h= this.high;
				else
					h= i.high;
				temp = new negInf(h);
			}
			else
			{
				l= i.low;
				if(this.high<i.high)
					h= this.high;
				else
					h=i.high;
				temp = new intInterval(l,h);
			}
		}
		else
			temp = new Bottom();
		return temp;
	}
	
	public boolean intersect(Interval b)
	{
        boolean r = false;
        if(b.high < this.low || b.low > this.high)
	        r = false;
        else if(this.low <= b.high && b.low <= this.high)
	        r = true;
        return r;
	}
}
