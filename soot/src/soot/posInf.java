package soot;
import java.io.*;

public class posInf extends Interval
{
    public PrintWriter writer;
    
	public posInf(int l)
	{
		super(l,2147483647);
		//low = l;
		//high = 2147483647;
	}
	
	public String getType()
	{
		return "posInf";
	}
	
	public int getLow()
	{
		return low;
	}
	
	public Interval clone()
	{
		Interval i = new posInf(low);
		return i;
	}
	
	public Interval join(Interval i)
	{
		Interval temp ;
		if(i.getType() == "Bottom")
			temp = this.clone();
		else if(i.getType() == "Top" || i.getType() == "negInf")
			temp = new Top();
		else 
		{
			if(i.low< this.low)
				temp = new posInf(i.low);
			else
				temp = new posInf(this.low);
		}
		return temp;
	}
    
    public Interval meet(Interval i)
	{

		Interval temp;
		int l,h;
		if(i.getType() == "Top")
			temp = this.clone();
		else if(i.getType() =="Bottom")
			temp = new Bottom();
		else if(intersect(i))
		{
			if(i.getType() == "posInf")
			{
				if(this.low < i.low)
					l= i.low;
				else
					l = this.low;
				temp = new posInf(l);
			}
			else if(i.getType() == "negInf")
			{
				l= this.low;
				h= i.high;
				temp = new intInterval(l,h);
			}
			else
			{
				if(this.low<i.low)
					l=i.low;
				else
					l= this.low;
				h= i.high;
				temp = new intInterval(l,h);
			}
		}
		else
			temp = new Bottom();
		return temp;
	}
	
    public Interval intersection(Interval i)
	{
        
        try {
            writer = new PrintWriter("/Users/Afsane/Result.txt");
        } catch (FileNotFoundException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		Interval temp;
		int l,h;
		if(i.getType() == "Top")
        {
			temp = this.clone();
            this.writer.println(temp +"cloned");
			this.writer.flush();
        }
		else if(i.getType() =="Bottom")
			temp = new Bottom();
		else if(intersect(i))
		{
			if(i.getType() == "posInf")
			{
				if(this.low < i.low)
					l= i.low;
				else
					l = this.low;
				temp = new posInf(l);
			}
			else if(i.getType() == "negInf")
			{
				l= this.low;
				h= i.high;
				temp = new intInterval(l,h);
			}
			else
			{
				if(this.low<i.low)
					l=i.low;
				else
					l= this.low;
				h= i.high;
				temp = new intInterval(l,h);
			}
		}
		else
			temp = new Bottom();
        this.writer.println(temp +"cloned");
        this.writer.flush();
		return temp;
	}
	
	public boolean intersect(Interval b) 
	{
	      boolean r = false;
	      if(b.high < this.low || b.low > this.high)
	        r = false;
	      else if(this.low <= b.high && b.low <= this.high)
	        r = true;
	      return r;
	}
}
