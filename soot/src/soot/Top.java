package soot;


public class Top extends Interval
{
	public Top()
	{
		super(-2147483648,2147483647);
		//low = -2147483648;
		//high = 2147483647;
	}
	
	public String getType()
	{
		return "Top";
	}
	
	public int getLow()
	{
		return low;
	}
	
	public int getHigh()
	{
		return high;
	}
	
	public Interval join(Interval i)
	{
		Interval temp = new Top();
		return temp;
	}
    
    public Interval meet(Interval i)
	{
		Interval temp =i.clone();
		return temp;
	}
    
	public Interval intersection(Interval i)
	{
		Interval temp =i.clone();
		return temp;
	}
	
	public Interval clone()
	{
		Interval i = new Top();
		return i;
	}
}
