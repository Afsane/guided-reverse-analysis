package soot;


public  class intInterval extends Interval
{
	public intInterval(int l, int h)
	{
		super();
		if(l<=h)
		{
			//super(l,h);
			low = l;
			high = h;
		}
		else
			System.out.println("Invalid: low is not <= to l");
	}
	
	public String getType()
	{
		return "intInterval";
	}
	
	public int getLow()
	{
		return low;
	}
	
	public int getHigh()
	{
		return high;
	}
	public Interval clone()
	{
		Interval i = new intInterval(low,high);
		return i;
	}
	
	public Interval join(Interval i)
	{
		int l,h;
		Interval temp ;
		if(i.getType() =="Top")
			temp = new Top();
		else if (i.getType() == "Bottom")
			temp = this.clone();
		else if(i.getType() == "negInf")
		{
			if(i.high > this.high)
				temp = new negInf(i.high);
			else
				temp = new negInf(this.high);
		}
		else if (i.getType() == "posInf")
		{
			if(i.low<this.low)
				temp = new posInf(i.low);
			else
				temp = new posInf(this.low);
		}
		else
		{
			if(i.low <this.low)
				l= i.low;
			else
				l= this.low;
			if(i.high>this.high)
				h= i.high;
			else
				h=this.high;
			temp = new intInterval(l,h);
		}
		return temp;
	}
    
    public Interval meet(Interval i)
	{
		int l,h;
		Interval temp ;
		if(i.getType() == "Top")
            temp =this.clone();
		else if(i.getType() == "Bottom")
			temp = new Bottom();
		else if(intersect(i))
		{
			if(i.getType() == "negInf")
			{
				l= this.low;
				if(this.high> i.high)
					h= i.high;
				else
					h= this.high;
				temp= new intInterval(l,h);
                
			}
			else if(i.getType() == "posInf")
			{
				h= this.high;
				if(this.low< i.low)
					l= i.low;
				else
					l= this.low;
				temp = new intInterval(l,h);
			}
			else
			{
				if(this.low< i.low)
					l= i.low;
				else
					l= this.low;
				if(this.high> i.high)
					h= i.high;
				else
					h= this.high;
				temp = new intInterval(l,h);
			}
		}
		else
			temp = new Bottom();
		return temp;
	}
	
	public Interval intersection(Interval i)
	{
		int l,h;
		Interval temp ;
		if(i.getType() == "Top")
			temp = this.clone();
		else if(i.getType() == "Bottom")
			temp = new Bottom();
		else if(intersect(i))
		{
			if(i.getType() == "negInf")
			{
				l= this.low;
				if(this.high> i.high)
					h= i.high;
				else
					h= this.high;
				temp= new intInterval(l,h);
                
			}
			else if(i.getType() == "posInf")
			{
				h= this.high;
				if(this.low< i.low)
					l= i.low;
				else
					l= this.low;
				temp = new intInterval(l,h);
			}
			else
			{
				if(this.low< i.low)
					l= i.low;
				else
					l= this.low;
				if(this.high> i.high)
					h= i.high;
				else
					h= this.high;
				temp = new intInterval(l,h);
			}
		}
		else
			temp = new Bottom();
		return temp;
	}
	
	public boolean intersect(Interval b)
	{
        boolean r = false;
        if(b.high < this.low || b.low > this.high)
	        r = false;
        else if(this.low <= b.high && b.low <= this.high)
	        r = true;
        return r;
	}
}
